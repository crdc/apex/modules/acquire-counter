import gi
import threading

gi.require_version('Apex', '1.0')

from gi.repository import Apex
from gi.repository import GLib, GObject

from threading import Thread
from uldaq import get_daq_device_inventory, DaqDevice, InterfaceType
from time import sleep
from sys import stdout
from os import system
from datetime import datetime
from counter import constants as c
from threading import Thread

running = False

class CounterStartJob(Apex.Job):
    __gtype_name__ = "CounterStartJob"

    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,)),
        'new_data': (GObject.SignalFlags.RUN_LAST, object, (object,))
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def do_task(self):
        global running
        running = True

        counter_number = 0
        interface_type = InterfaceType.USB
        daq_device = None

        try:
            # Get descriptors for all of the available DAQ devices.
            devices = get_daq_device_inventory(interface_type)
            number_of_devices = len(devices)

            # Verify at least one DAQ device is detected.
            if number_of_devices == 0:
                raise Exception('Error: No DAQ devices found')

            # Apex.debug("Found %d DAQ device(s)", number_of_devices)
            # for i in range(number_of_devices):
                # Apex.debug("    %s (%s)", devices[i].product_name, devices[i].unique_id)

            # Create a DaqDevice object from the first descriptor.
            daq_device = DaqDevice(devices[0])
            ctr_device = daq_device.get_ctr_device()

            # Verify the specified DAQ device supports counters.
            if ctr_device is None:
                raise Exception('Error: The DAQ device does not support counters')

            descriptor = daq_device.get_descriptor()
            # Apex.debug("Connecting to %s", descriptor.dev_string)
            # Establish a connection to the device.
            daq_device.connect()

            ctr_info = ctr_device.get_info()
            dev_num_counters = ctr_info.get_num_ctrs()
            if counter_number >= dev_num_counters:
                counter_number = dev_num_counters - 1

            ctr_device.c_clear(counter_number)

            data = {
                "counter_number": counter_number,
                "ctr_device": ctr_device,
                "daq_device": daq_device
            }

            GLib.timeout_add_seconds(1, self.on_delay_timeout, data)

        except Exception as e:
            print('\n', e)

    def on_delay_timeout(self, user_data):
        global running

        if (running):
            ctr_device = user_data["ctr_device"]
            counter_number = user_data["counter_number"]

            try:
                # Read and display the data.
                counter_value = ctr_device.c_in(counter_number)
                self.emit("new_data", counter_value)
                Apex.debug("count: %d" % counter_value)

            except (ValueError, NameError, SyntaxError):
                Apex.critical("Error reading the counter device ")

            return True
        else:
            dev = user_data["daq_device"]
            if dev:
                # Disconnect from the DAQ device.
                if dev.is_connected():
                    dev.disconnect()
                # Release the DAQ device resource.
                dev.release()

            return False

class CounterStopJob(Apex.Job):
    __gtype_name__ = "CounterStopJob"

    __gsignals__ = {
        'event': (GObject.SignalFlags.RUN_LAST, object, (object,)),
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def do_task(self):
        global running
        running = False

        Apex.debug("stop running")

        event = Apex.Event.new_full(c.E_STOP,
                                    "stop",
                                    "stop execution")
        self.emit("event", event)
        running = False
