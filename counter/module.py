import gi

#pylint: enable=wrong-import-position

gi.require_version('Apex', '1.0')

#pylint: disable=wrong-import-position

from gi.repository import Apex
from gi.repository import GLib, GObject

# from counter.model import AppModel
from counter.job import CounterStartJob, CounterStopJob

class App(Apex.Application):
    """App class is the module runner"""
    __gtype_name__ = "App"

    __gsignals__ = {
        'stop': (GObject.SignalFlags.RUN_LAST, object, (object,))
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args,
                         application_id="org.apex.acquire.Counter",
                         flags=0,
                         **kwargs)
        self.set_endpoint("tcp://localhost:7201")
        self.set_service("acquire-counter")
        self.set_inactivity_timeout(10000)
        self.broker = None
        self.__setup_broker("tcp://localhost:7200")

        self.table = None
        self.header = None
        self.metric_source = None
        self.metric = None
        self.__setup_metrics()

        self.event_source = None
        self.__setup_events("tcp://localhost:11005", "")

        self.connected = 0
        self.__setup_properties()

    def __setup_broker(self, endpoint):
        """Connect to the REQ/REP broker at the given endpoint."""
        self.broker = Apex.Client.new(endpoint)

    def __setup_metrics(self):
        self.table = Apex.MetricTable.new()
        self.header = Apex.MetricTableHeader.new("calibration")
        self.header.add_column("counter_x0", 0.0)
        self.header.add_column("counter_x1", 1.0)
        self.table.set_name("counter")
        self.table.set_header(self.header)
        self.metric_source = Apex.Source.new("tcp://localhost:13101", "")
        self.add_source("metric", self.metric_source)

    def __setup_events(self, endpoint, msg_filter):
        self.event_source = Apex.Source.new(endpoint, msg_filter)
        self.add_source("event", self.event_source)

    def __setup_properties(self):
        self.connected = Apex.Property.new("connected", "false")
        self.add_property(self.connected)

    def on_publish_event(self, user_data, event):
        """Send an event to the connected bus sink for event messages"""
        Apex.debug("sending event: %s" % event.serialize())
        try:
            self.send_event(event)
        except GLib.Error as err:
            if err.matches(Apex.apex_error_quark(),
                           Apex.ApexErrorEnum.NOT_CONNECTED):
                Apex.error('Event source not connected')
            else:
                raise

    def on_new_data(self, user_data, value):
        dt = GLib.DateTime.new_now_local()
        year = dt.get_year()
        mon = dt.get_month()
        day = dt.get_day_of_month()
        h = dt.get_hour()
        m = dt.get_minute()
        s = dt.get_second()
        u = dt.get_microsecond()
        ts = '{}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}.{}'.format(year, mon, day, h, m, s, u)
        self.table.set_timestamp(ts)
        self.table.add_entry("count", value)
        metric = Apex.Metric.new_full(GLib.uuid_string_random(), "log-data", self.table)
        self.send_metric(metric)

    def do_activate(self):
        self.metric_source.start()

    def do_shutdown(self):
        self.metric_source.stop()

    # apex_application_submit_job override
    def do_submit_job(self, job_name, job_value, job_properties):
        Apex.message("submit-job: %s [%s]" % (job_name, job_value))
        response = Apex.JobResponse.new()
        if job_name == "start":
            Apex.message('job = {:s}'.format(job_name))
            job = CounterStartJob()
            job.connect("event", self.on_publish_event)
            job.connect("new-data", self.on_new_data)
            self.update_property("connected", "true")
            response.set_job(job)
        elif job_name == "stop":
            Apex.message('job = {:s}'.format(job_name))
            job = CounterStopJob()
            job.connect("event", self.on_publish_event)
            self.update_property("connected", "false")
            response.set_job(job)
        else:
            pass
        return response
