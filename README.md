# Apex Counter Module

A `plantd` module for counting pulses using a Measurement Computing USB-202 device.

This has two jobs that is can run.

 * `start`: locates and connects to the DAQ device and polls the counter once every second
 * `stop`: stops polling the USB device

 A `metric` source is given the counter value that gets sent to the broker.
 The endpoints are hard coded as follows

  * metric source: `tcp://localhost:13101`
  * events source (not used): `tcp://localhost:11005`
  * module: `tcp://localhost:7201`
  * broker: `tcp://localhost:7200`

The metrics are not scaled (ie. slope =1, offset = 0)

## Setup

```sh
sudo apt-get install gcc g++ make
sudo apt-get install libusb-1.0-0-dev
wget https://github.com/mccdaq/uldaq/releases/download/v1.1.1/libuldaq-1.1.1.tar.bz2
tar -xvjf libuldaq-1.1.1.tar.bz2
libuldaq-1.1.1
./configure && make
sudo make install
```

## Install

```sh
python3 setup.py sdist
sudo python3 -m pip install dist/acquire-counter-0.1.2.dev0+g934c689.d20190717.tar.gz
```

## Developing

```sh
virtualenv env
source env/bin/activate
pip install -r requirements.txt -e .
```

## Debugging with plantctl

Add the following lines to /etc/default/plantd/modules/acquire-counter

```sh
G_MESSAGES_DEBUG=all
PLANTD_MODULE_ARGS=-vvv
``

## Other

May need to do this to fix libapex (bug already reported)

```sh
sudo cp /usr/src/libapex/apex/Apex.py /usr/lib/python3/dist-packages/gi/overrides/
```
