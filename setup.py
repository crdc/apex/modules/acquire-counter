from setuptools import setup, find_packages

setup(
    name='acquire-counter',
    use_scm_version=True,
    author='Stephen Roy',
    author_email='stephen.roy@coanda.ca',
    description='Apex acquire module for a USB-202 counter',
    packages=find_packages(),
)

